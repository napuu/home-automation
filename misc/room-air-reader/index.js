const fs = require('fs');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

const port = new SerialPort("/dev/ttyUSB0", { baudRate: 9600 });
const parser = new Readline();

port.pipe(parser);

const CURRENT_PATH = '/home/pi/room-air-reader';

parser.on('data', (line) => {
	console.log("> read line: " + line);
	const value = line.split(':')[1];
	let filename = '.tmp';
	if (line.indexOf('temp') !== -1) {
		filename = CURRENT_PATH + '/temperature';
	} else if (line.indexOf('hum') !== -1) {
		filename = CURRENT_PATH + '/humidity';
	}
	fs.writeFileSync(filename, value);
});
