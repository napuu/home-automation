
#include <DHT.h>;
#define DHTPIN 7
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

float hum;
float temp;

void setup()
{
  Serial.begin(9600);
  dht.begin();
}

void loop()
{
    delay(2000);
    hum = dht.readHumidity();
    temp= dht.readTemperature();
    Serial.print("hum:");
    Serial.println(hum);
    Serial.print("temp:");
    Serial.println(temp);
    delay(10000);
}

   
